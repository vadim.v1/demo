from base.locators import ProdcutPageLocators
from base.selenium_base import SeleniumBase


class ProductPage(SeleniumBase):

    def specs_button_is_visible(self):
        specs_button = self.is_visible(*ProdcutPageLocators.SPECS)
        return specs_button

    def specs_tab_is_visible(self):
        specs_tab = self.is_visible(*ProdcutPageLocators.SPECS_TAB)
        return specs_tab

    def click_on_request_a_quote_button(self):
        button = self.is_visible(*ProdcutPageLocators.REQUEST_A_QUOTE_BUTTON)
        button.click()

    def check_quote_counter(self):
        counter = self.is_visible(*ProdcutPageLocators.QUOTE_COUNTER)
        assert counter.text == "1"
