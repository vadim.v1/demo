from base.locators import HomePageLocators
from base.selenium_base import SeleniumBase
from selenium.webdriver.remote.webelement import WebElement


class HomePage(SeleniumBase):

    def login_button_is_present(self):
        search = self.is_present(*HomePageLocators.LOGIN_BUTTON_IN_HEADER)
        return search

    def login_button_is_visible(self):
        search = self.is_visible(*HomePageLocators.LOGIN_BUTTON_IN_HEADER)
        return search

    def menu_items_desktop_are_present(self):
        menu_items = self.are_present(*HomePageLocators.MENU_ITEMS_DESKTOP)
        return menu_items

    def menu_items_desktop_are_visible(self):
        menu_items = self.are_visible(*HomePageLocators.MENU_ITEMS_DESKTOP)
        return menu_items

    def menu_items_are_correct(self):
        menu_items = self.are_visible(*HomePageLocators.MENU_ITEMS_DESKTOP)
        expected_items = HomePageLocators.EXPECTED_MENU_ITEMS
        self.lists_comparison(expected_items, menu_items)






