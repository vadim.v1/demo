import pytest
from pages.prodcut_page import ProductPage
import time


class TestProductPage:
    @pytest.mark.parametrize("link", [
        "https://www.enterasource.com/dell-net-1m-40gbase-cr4-passive-direct-attach-twinax-cable-5np8r",
        "https://www.enterasource.com/dell-emc-poweredge-r250-4-bay-3-5-1u-rackmount-server",
        "https://www.enterasource.com/cisco-1921-sec-k9-1900-series-router"])
    def test_specs_tab_is_visible(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        page.specs_button_is_visible()
        page.specs_tab_is_visible()
    @pytest.mark.xfail
    def test_quotes_counter(self, driver):
        link = "https://www.enterasource.com/intel-xeon-e5-4830-v3-2-1ghz-12-core-30mb-8gt-s-115w-processor"
        page = ProductPage(driver, link)
        page.open()
        page.click_on_request_a_quote_button()
        page.check_quote_counter()
