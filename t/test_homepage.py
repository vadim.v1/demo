import pytest
import time
from pages.home_page import HomePage
from pages.login_page import LoginPage


# @pytest.mark.usefixtures("driver")
class TestHomepage:
    def test_login_button(self, driver):
        link = "https://www.enterasource.com/"
        page = HomePage(driver, link)
        page.open()
        page.login_button_is_present()
        page.login_button_is_visible().click()
        login_page = LoginPage(driver, driver.current_url)
        login_page.should_be_login_url()

    def test_elements_of_menu(self, driver):
        link = "https://www.enterasource.com/"
        page = HomePage(driver, link)
        page.open()
        page.menu_items_desktop_are_present()
        page.menu_items_are_correct()  # are_visible already inside






