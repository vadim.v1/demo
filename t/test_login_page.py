import pytest
from pages.home_page import HomePage
from pages.login_page import LoginPage


class TestLoginPage:
    @pytest.mark.xfail
    def test_user_login_1(self, driver):  # empty fields
        link = "https://dev:dev@enm2.m2.stage.interactivated.me/customer/account/login/"
        page = LoginPage(driver, link)
        page.open()
        page.click_on_login_button()
        page.email_error_is_correct()
        page.pass_error_is_correct()
